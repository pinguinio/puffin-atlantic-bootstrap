<div class="col-md-8">

	<h1>Hi there.</h1>

	<h3>
		At some point content jumped the damn fence and wandered off in new
		directions.
	</h3>

	<h3>
		CMSes are amazing tools. But they're just tools. They don't create ideas
		any more than a shovel, by itself, creates ditches.
	</h3>

	<h3>
		If you want an interesting website you have to actually BE interesting. No
		wordpress plugin can make up the the lack of depth, of substance&mdash; OF SOUL.
	</h3>

	<h3>
		Content is king. The old institutions of management are creaking in
		their dusty foundations. We aren't the revolution. We're the revolution
		that comes AFTER the revolution.
	</h3>

	<h3>
		Now, go create.
	</h3>
</div>
